#include <bits/stdc++.h>
#include <tuple>
#include <utility>
#include "complete_algo.hpp"
#include "./Utilities/util.hpp"

using namespace std;

bool euler_validation(vector <pair<int,int>> factors, int k) {
    for(pair<int,int> x : factors){
        if(!Euler_criterion(k % x.first,x.first)){
            return false;
        }   
    }
    return true;
}

int find_z (vector <pair<int, int>> factors, int k) {
    int rem[factors.size()], num[factors.size()];
    int index = 0;
            
    for(auto factor : factors){
        int ans = cubic_residue(k % factor.first,factor.first);
        //cout << factor.first << setw(10) << factor.second << endl;
        if(factor.second > 1){
            //cout << factor.first << setw(10) << factor.second << "#########" << endl;
            ans = powers_of_prime(factor.first,factor.second,k,ans);
            //cout << "Powers done" << endl;
        }
        //cout << "Out of if" << endl;
        rem[index] = ans;
        num[index++] = (int)pow(factor.first,factor.second);
        //cout << "Rem and num init done" << endl;
    }
    //cout << "rem and num done" << endl;
    return findMinX(num,rem,factors.size());
}

tuple <long, long, long> update_and_solve(int t, int z, int k) {
    pair <int, int> updated_soln = update(t, z);
    //cout << "The updated values are " << updated_soln.first << " " << updated_soln.second << endl;
    if (integral_solns(t, z, k)) {
        //cout << "Integral roots" << endl;
        pair <long, long> x_y = x_and_y(t, z, k);
        return make_tuple(x_y.first, x_y.second, z);
    }        
    
    return make_tuple(0, 0, 0);
}

int main(int argc, char * argv[]) {   
    int k = stoi(argv[1]);
    const long n = 100;
    
    tuple <int, int, int, bool> small_soln = special_case(k);
    cout << "(" << get<0>(small_soln) << ", " << get<1>(small_soln)
              << ", " << get<2>(small_soln) << ", " << get<3>(small_soln) 
              << ")" << endl;

    if (eligible(k)) {
        for (tuple <long, long, long> p: solutions(k)) {
            cout << "(" << get<0>(p) << ", " << get<1>(p)
              << ", " << get<2>(p) << ")" << endl;
        }
    }
    vector <int> s = seive(n);
    for(int t = 2; t < n ; t++){
        vector<pair<int,int>> factors = getFactorization(t, s);
        //cout << t << endl;
        //cout << "Euler validation" << endl;
        if(euler_validation(factors, k)) {
            // cout << "Euler validation done" << endl;
            int z = find_z(factors, k);
            //cout << "findMInX done" << endl;
            tuple<long, long, long> soln = update_and_solve(t, z, k);
            if (get<0>(soln) == 0 && get<1>(soln) == 0 && get<2>(soln) == 0) {
                cout << "No solution for z = " << z << setw(10) << t << endl;
            }
            else {
                cout << "The solution is: " << endl;
                cout << "(" << get<0>(soln) << ", " << get<1>(soln)
                    << ", " << get<2>(soln) << ")" << endl;
            }
        }
        else {
            cout << "No solution for t = " << t << endl;
        }
    }
    return 0; 
}