#include <bits/stdc++.h>
#include "./Utilities/util.hpp"

using namespace std;

// If eligible value of k, return solutions

struct xyz{
    long x,y,z;
    bool found ;
};

tuple <int, int, int, bool> special_case(int k){
    int B_k = ((2 + sqrt(12 * k - 3))/6);
    tuple <int, int, int, bool> values = make_tuple(0, 0, 0, false);
    for(int x = -B_k ; x <= B_k ; x++){
        for(int y = x; y <= B_k; y++){
            for(int z = y; z <= B_k; z++){
                if(x*x*x + y*y*y + z*z*z == k)
                {
                    values = make_tuple(x, y, z, false);
                    return values;
                }

            }
        }
    }
    return values;
}

bool eligible(int k) {
    return k % 3 == 0;
}

bool integral_roots(float a, float b, float c) {
    float determinant = sqrt(pow(b, 2) - 4 * a * c);
    return (determinant == float(int(determinant))) && ((int(-b + determinant) % int(2 * a)) == 0);
}

vector <tuple <long, long, long>> solutions(int k) {
    vector <tuple <long, long, long>> x_y_z;
    for (long z: factors(k / 3)) {
        float a = 1.0, b = z, c = k / (z * 3.0);
        if (integral_roots(a, b, c)) {
            pair <long, long> roots = quadratic_roots(a, b, c);
            x_y_z.push_back(make_tuple(roots.first, roots.second, z));
        }
    }
    return x_y_z;    
}