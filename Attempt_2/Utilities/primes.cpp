#include <bits/stdc++.h>

using namespace std;

vector <int> seive_intialization(int bound){
    vector<int> spf(bound, 1);
    for(int i = 1; i < bound; i++)
        spf[i] = i;
    
    for (int i = 4; i < bound; i += 2)
        spf[i] = 2; 
    return spf;
}

vector <int> seive(int bound){
    vector<int> spf = seive_intialization(bound);

    for (int i=3; i*i < bound ; i++) { 
        if (spf[i] == i) { 
            // if i is a prime  
            for (int j=i*i; j < bound; j+=i) {
                if (spf[j]==j) 
                    spf[j] = i; 
            }      
        } 
    } 
    return spf;
}

vector<pair<int,int>> getFactorization(int x,vector<int> spf) { 
    
    vector<pair<int,int>> factors; 
    int prev = 1,count = 1;
    
    while (x != 1) 
    { 
        if(spf[x] != prev){
            if(prev != 1)
              factors.push_back({prev,count});
            count = 1;
            prev = spf[x];
        }
        else{
            count++;
        }
        x = x / spf[x]; 
         
    } 
    
    factors.push_back({prev,count});
    return factors;   
} 

int gcd(int a, int b) { 
    if (b == 0) 
        return a; 
    if (a == 0)
        return b;
    return gcd(b, a % b);        
} 

bool Euler_criterion(int a,int prime){
    int q = gcd(prime-1,3);
    return ((int)pow(a,(prime -1)/q) % prime == 1);
}